require 'yaml'

module Wdbot
  autoload :Database, 'mark'
  autoload :Comparator, 'comparator'
  autoload :Parser, 'parser'
  autoload :Notifier, 'notifier'
  class AGHBot
    
    def self.process(settings_file)
      settings = YAML.load(File.open(settings_file))
      @parser = Parser.new('https://dziekanat.agh.edu.pl',settings['login'],settings['password'])
      Database.new(settings['db_path'])
      diffs = Comparator.differences(@parser.marks)
      #pp diffs
      notifier = Notifier.new(diffs)
      notifier.send_email(settings)
    end
  end
end