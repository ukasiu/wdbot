require 'nokogiri'
require 'mechanize'

module Wdbot

  # http://stackoverflow.com/questions/8749101/parse-html-table-using-nokogiri-and-mechanize
  class TableExtractor  
    def self.extract_data html
      html.xpath('//table[@id="ctl00_ctl00_ContentPlaceHolder_RightContentPlaceHolder_dgDane"]//tr[@class!="gridDaneHead"]').collect do |row|
        subject   = row.at("td[1]").text.strip
        type = row.at("td[2]").text.strip
        mark_field = row.at("td[3]").inner_html.to_s
        mark, date = mark_field.split('<br>') if mark_field.match("<br>")
        mark = Nokogiri::HTML(mark).text if mark
        mark = nil if mark and mark.match(/^\s+$/)
        date = Date.strptime(Nokogiri::HTML(date).text,'%d.%m.%y') if date
        {:subject => subject, :mark => mark, :date => date, :type => type}
      end
    end
  end

  class Parser

    attr_accessor :agent

    def initialize(instance_url,login,password)
      @agent = Mechanize.new
      @agent.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE # problem with AGH cerificate

      @instance_url = instance_url
      @login = login
      @password = password

      self.login
    end

    def form_login 
      #TODO do login this way
      #submitting form doesn't work
      #after using this it may work: https://gist.github.com/kevinthompson/5788979
      login_page = @agent.get(@instance_url + '/')
      login_form = login_page.form('aspnetForm')
      login_form['ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$txtIdent'] = @login
      login_form['ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$txtHaslo'] = @password
      login_form.radiobuttons_with(:name => 'ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$rbKto')[0].check

      login_action = @agent.submit(login_form)
    end

    def login
      params = {
        'ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$txtIdent' => @login,
        'ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$txtHaslo' => @password,
        'ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$rbKto' => 'student',
        'ctl00$ctl00$ContentPlaceHolder$MiddleContentPlaceHolder$butLoguj' => 'Zaloguj',
        'ctl00_ctl00_TopMenuPlaceHolder_TopMenuContentPlaceHolder_MenuTop3_menuTop3_ClientState' => '',
        'ctl00_ctl00_ScriptManager1_HiddenField' => '',
        '__EVENTTARGET' => '',
        '__EVENTARGUMENT' => ''
      }
      login_action = @agent.post(@instance_url + '/Logowanie2.aspx', params)
    end

    def marks
      marks_page = @agent.get(@instance_url + '/OcenyP.aspx')
      marks_table_dom = marks_page.search('//table')
      marks_table = TableExtractor.extract_data marks_table_dom
    end
  end
end