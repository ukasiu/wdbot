# encoding: UTF-8

require 'data_mapper'
require 'dm-sqlite-adapter'

module Wdbot
  class Database
    class Mark
      include DataMapper::Resource

      property :id, Serial
      property :subject, String
      property :type, String
      property :mark, String
      property :date, DateTime
      def full_name
        "#{subject} – #{type} – #{date.strftime("%d.%m.%Y")}"
      end
    end
    def initialize(db_path)
      DataMapper.setup(:default, "sqlite://#{db_path}")
      DataMapper.finalize
      DataMapper.auto_upgrade!
    end
  end
end

