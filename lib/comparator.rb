require 'date'

module Wdbot
  class Comparator 
    def self.differences(marks)
      diffs = []
      marks.each do |mark| 
        if mark[:mark]
          search_attrs = {:subject => mark[:subject], :type => mark[:type]}
          count = Wdbot::Database::Mark.all(search_attrs).count #TODO do it better
          if count > 1
            raise "Not expected"
          elsif count == 1
            stored_mark = Wdbot::Database::Mark.first(search_attrs)
            if stored_mark[:mark] != mark[:mark]
              diffs << "Changed result: #{stored_mark.full_name}: from #{stored_mark[:mark]} to #{mark[:mark]}"
            end
            stored_mark.update(:mark => mark[:mark])
          else
            mark = Wdbot::Database::Mark.create(mark)
            diffs << "New result: #{mark.full_name}: #{mark[:mark]}"
          end
        end
      end
      diffs
    end
  end
end