require 'mail'

module Wdbot
  class Notifier
    def initialize(diffs)
      @diffs = diffs
    end
    def send_email(settings)
      body = @diffs.join("\n")
      if @diffs.count > 0
        Mail.deliver do
          delivery_method :smtp, {
            :address => settings['smtp_server'],
            :port => settings['smtp_port'],
            :user_name => settings['smtp_username'],
            :password => settings['smtp_password'],
            :enable_ssl => true
          }
          from     settings['email_from']
          to       settings['email_to']
          subject  'Changes in your marks'
          body     body
        end
      end
    end
  end
end