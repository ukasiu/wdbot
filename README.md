# wdbot

Bot providing notfications of new marks in Wirtualny Dziekanat's instances.

## How to install
Just run `rake install`

## How to use (AGH instance)

Install gem and create a file settings.yml:
```
login: "" # usually transcirpt number 
password: "" 
db_path: "" # must be absolute
email_to: [""] # email adresses to send notifications to
email_from: "" # email "From" address
smtp_server: smtp.gmail.com # smtp server address
smtp_username: "" # smtp username
smtp_password: "" # smtp password
smtp_port: "" # smtp port
```
and bot.rb:
```
require 'wdbot'

Wdbot::AGHBot.process('settings.yml')
```

## Contributing to wdbot
 
* Check out the latest master to make sure the feature hasn't been implemented or the bug hasn't been fixed yet.
* Check out the issue tracker to make sure someone already hasn't requested it and/or contributed it.
* Fork the project.
* Start a feature/bugfix branch.
* Commit and push until you are happy with your contribution.
* Make sure to add tests for it. This is important so I don't break it in a future version unintentionally.
* Please try not to mess with the Rakefile, version, or history. If you want to have your own version, or is otherwise necessary, that is fine, but please isolate to its own commit so I can cherry-pick around it.

## Copyright

Copyright (c) 2014 Łukasz Gurdek. See LICENSE.txt for
further details.

